package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"golang.org/x/crypto/bcrypt"
	"growexams/growexams-backend/db"
	"growexams/growexams-backend/graph/generated"
	"growexams/growexams-backend/graph/model"
	"growexams/growexams-backend/services"
)

func (r *mutationResolver) CreateSubject(ctx context.Context, input model.CreateSubject) (*model.Status, error) {
	if service.CheckAdminJWT(input.Token) {
		subject := &model.Subject{
			Code: input.Code,
			Name: input.Name,
		}
		db.MySQLConn.Create(subject)
		token, _ := r.RefreshToken(ctx, model.RefreshToken{Token: input.Token})
		return &model.Status{
			Token: token.Token,
			Msg:   "",
			Code:  201,
		}, nil
	}
	return nil, fmt.Errorf("wrong jwt")
}

func (r *mutationResolver) CreateCourse(ctx context.Context, input model.CreateCourse) (*model.Status, error) {
	if service.CheckAdminJWT(input.Token) {
		course := &model.Course{
			Group:    input.Group,
			Semester: &(input.Semester),
			Year:     &(input.Year),
		}
		db.MySQLConn.Create(course)
		token, _ := r.RefreshToken(ctx, model.RefreshToken{Token: input.Token})
		return &model.Status{
			Token: token.Token,
			Msg:   "",
			Code:  201,
		}, nil
	}
	return nil, fmt.Errorf("wrong jwt")
}

func (r *mutationResolver) CreateExam(ctx context.Context, input model.CreateExam) (*model.Status, error) {
	// if service.CheckAdminJWT(input.Token) {
	// 	subject := &model.Subject{
	// 		Code: input.,
	// 		Name: input.Name,
	// 	}
	// 	db.MySQLConn.Create(subject)
	// 	token, err = r.RefreshToken(ctx, model.RefreshToken{Token: input.Token})
	// 	return &model.Status{
	// 		Token: token,
	// 		msg:   "",
	// 		Code:  201,
	// 	}, nil
	// }
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) CreateExamResult(ctx context.Context, input model.CreateExamResult) (*model.Status, error) {
	// if service.CheckAdminJWT(input.Token) {
	// 	subject = &model.Subject{
	// 		Code: input.Code,
	// 		Name: input.Name,
	// 	}
	// 	db.MySQLConn.Create(subject)
	// 	token, err = r.RefreshToken(ctx, model.RefreshToken{Token: input.Token})
	// 	return &model.Status{
	// 		Token: token,
	// 		msg:   "",
	// 		Code:  201,
	// 	}, nil
	// }
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) CreateStudent(ctx context.Context, input model.CreateStudent) (*model.Status, error) {
	if service.CheckAdminJWT(input.Token) {
		hash, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
		student := &model.Student{
			Name:     input.Name,
			Username: input.Username,
			Email:    input.Email,
			Password: string(hash),
		}
		db.MySQLConn.Create(student)
		token := service.GenJWT(student.Username, student.Email, "student")
		return &model.Status{
			Token: token,
			Msg:   "",
			Code:  200,
		}, nil
	}
	err := fmt.Errorf("Invalid Token")
	return nil, err
}

func (r *mutationResolver) CreateTeacher(ctx context.Context, input model.CreateTeacher) (*model.Status, error) {
	if service.CheckAdminJWT(input.Token) {
		hash, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
		teacher := &model.Teacher{
			Name:     input.Name,
			Username: input.Username,
			Email:    input.Email,
			Password: string(hash),
		}
		db.MySQLConn.Create(teacher)
		token := service.GenJWT(teacher.Username, teacher.Email, "teacher")
		return &model.Status{
			Token: token,
			Msg:   "",
			Code:  200,
		}, nil
	}
	err := fmt.Errorf("Invalid Token")
	return nil, err
}

func (r *mutationResolver) CreateAdmin(ctx context.Context, input model.CreateAdmin) (*model.Status, error) {
	if service.CheckAdminJWT(input.Token) {
		hash, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
		admin := &model.Admin{
			Name:     input.Name,
			Username: input.Username,
			Email:    input.Email,
			Password: string(hash),
		}
		db.MySQLConn.Create(admin)
		token := service.GenJWT(admin.Username, admin.Email, "admin")
		return &model.Status{
			Token: token,
			Msg:   "",
			Code:  200,
		}, nil
	}
	err := fmt.Errorf("Invalid Token")
	return nil, err
}

func (r *mutationResolver) StudentLogin(ctx context.Context, input model.StudentLogin) (*model.Status, error) {
	student := &model.Student{}
	db.MySQLConn.Where("email = ?", input.Email).Find(student)
	err := bcrypt.CompareHashAndPassword([]byte(student.Password), []byte(input.Password))
	if err != nil {
		return nil, err
	}
	token := service.GenJWT(student.Username, student.Email, "student")
	return &model.Status{
		Token: token,
		Msg:   "",
		Code:  200,
	}, nil
}

func (r *mutationResolver) TeacherLogin(ctx context.Context, input model.TeacherLogin) (*model.Status, error) {
	teacher := &model.Teacher{}
	db.MySQLConn.Where("email = ?", input.Email).Find(teacher)
	err := bcrypt.CompareHashAndPassword([]byte(teacher.Password), []byte(input.Password))
	if err != nil {
		return nil, err
	}
	token := service.GenJWT(teacher.Username, teacher.Email, "teacher")
	return &model.Status{
		Token: token,
		Msg:   "",
		Code:  200,
	}, nil
}

func (r *mutationResolver) AdminLogin(ctx context.Context, input model.AdminLogin) (*model.Status, error) {
	admin := &model.Admin{}
	db.MySQLConn.Where("email = ?", input.Email).Find(admin)
	err := bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(input.Password))
	if err != nil {
		return nil, err
	}
	token := service.GenJWT(admin.Username, admin.Email, "admin")
	return &model.Status{
		Token: token,
		Msg:   "",
		Code:  200,
	}, nil
}

func (r *mutationResolver) RefreshToken(ctx context.Context, input model.RefreshToken) (*model.Status, error) {
	isToken, username, email, userType := service.CheckJWT(input.Token)
	if isToken {
		token := service.GenJWT(username, email, userType)
		return &model.Status{
			Token: token,
			Msg:   "",
			Code:  200,
		}, nil
	}
	return nil, fmt.Errorf("Invalid Token")
}

func (r *queryResolver) Student(ctx context.Context, id int, token string) (*model.Student, error) {
	student := &model.Student{
		Name:     "Juan Manuel Valencia",
		Username: "jmvalenciz",
		Email:    "jmvalenciz@eafit.edu.co",
	}
	r.Query()
	requestedFields := graphql.CollectFieldsCtx(ctx, nil)

	for _, field := range requestedFields {
		if field.Name == "courses" {

		}
	}
	return student, nil
}

func (r *queryResolver) Teacher(ctx context.Context, id int, token string) (*model.Teacher, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Admin(ctx context.Context, id int, token string) (*model.Admin, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Course(ctx context.Context, id int, token string) (*model.Course, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Courses(ctx context.Context, token string) ([]*model.Course, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Students(ctx context.Context) ([]*model.Student, error) {
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
