// // Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

// type Admin struct {
// 	ID          uint   `json:"id" gorm:"primaryKey"`
// 	Name        string `json:"name"`
// 	Username    string `json:"username"`
// 	Email       string `json:"email"`
// 	Permissions string `json:"permissions"`
// 	Password    string `json:"-"`
// }

// type AdminLogin struct {
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }

// type Answer struct {
// 	ID            uint        `json:"id" gorm:"primaryKey"`
// 	CorrectAnswer string      `json:"correctAnswer"`
// 	GivenAnswer   string      `json:"givenAnswer"`
// 	ExamResult    *ExamResult `json:"exam"`
// 	ExamResultID  uint        `json:"-"`
// }

// type Course struct {
// 	ID          uint          `json:"id" gorm:"primaryKey"`
// 	Group       string        `json:"group"`
// 	Year        *int          `json:"year"`
// 	Semester    *int          `json:"semester"`
// 	Subject     *Subject      `json:"subject" gorm:"foreignKey:ID"`
// 	Exams       []*Exam       `json:"exams" gorm:"many2many:courses_exams;"`
// 	Teachers    []*Teacher    `json:"teachers" gorm:"many2many:courses_teachers;"`
// 	Students    []*Student    `json:"students" gorm:"many2many:courses_students;"`
// 	ExamResults []*ExamResult `json:"examResults" gorm:"foreignKey:ID"`
// }

// type CreateAdmin struct {
// 	Token    string `json:"token"`
// 	Name     string `json:"name"`
// 	Username string `json:"username"`
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }

// type CreateCourse struct {
// 	Token    string `json:"token"`
// 	Group    string `json:"group"`
// 	Year     int    `json:"year"`
// 	Semester int    `json:"semester"`
// 	Subject  int    `json:"subject"`
// }

// type CreateExam struct {
// 	Token     string `json:"token"`
// 	Name      string `json:"name"`
// 	StartDate string `json:"start_date"`
// 	EndDate   string `json:"end_date"`
// }

// type CreateExamResult struct {
// 	Token string `json:"token"`
// 	Title string `json:"title"`
// 	Exam  int    `json:"exam"`
// }

// type CreateStudent struct {
// 	Token    string `json:"token"`
// 	Name     string `json:"name"`
// 	Username string `json:"username"`
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }

// type CreateSubject struct {
// 	Token string `json:"token"`
// 	Code  string `json:"code"`
// 	Name  string `json:"name"`
// }

// type CreateTeacher struct {
// 	Token    string `json:"token"`
// 	Name     string `json:"name"`
// 	Username string `json:"username"`
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }

// type Exam struct {
// 	ID        uint        `json:"id" gorm:"primaryKey"`
// 	Name      string      `json:"name"`
// 	StartDate string      `json:"start_date"`
// 	EndDate   string      `json:"end_date"`
// 	Questions []*Question `json:"questions" gorm:"foreignKey:ID"`
// }

// type ExamResult struct {
// 	ID        uint      `json:"id" gorm:"primaryKey"`
// 	Title     string    `json:"title"`
// 	Exam      *Exam     `json:"exam"`
// 	Course    *Course   `json:"course"`
// 	Student   *Student  `json:"student"`
// 	Answers   []*Answer `json:"answers" gorm:"foreignKey:ID"`
// 	ExamID    uint      `json:"-"`
// 	CourseID  uint      `json:"-"`
// 	StudentID uint      `json:"-"`
// }

// type NewSubject struct {
// 	Code string `json:"code"`
// 	Name string `json:"name"`
// }

// type Question struct {
// 	ID        uint   `json:"id" gorm:"primaryKey"`
// 	Content   string `json:"content"`
// 	Formula   string `json:"formula"`
// 	Variables string `json:"variables"`
// 	Exam      *Exam  `json:"exam"`
// 	ExamID    uint   `json:"-"`
// }

// type RefreshToken struct {
// 	Token string `json:"token"`
// }

// type Status struct {
// 	Token string `json:"token"`
// 	Code  int    `json:"code"`
// 	Msg   string `json:"msg"`
// }

// type Student struct {
// 	ID          uint          `json:"id" gorm:"primaryKey"`
// 	Name        string        `json:"name"`
// 	Username    string        `json:"username"`
// 	Email       string        `json:"email"`
// 	Courses     []*Course     `json:"courses" gorm:"many2many:courses_students;"`
// 	ExamResults []*ExamResult `json:"examResults" gorm:"foreignKey:ID"`
// 	Password    string        `json:"-"`
// }

// type StudentLogin struct {
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }

// type Subject struct {
// 	ID       uint       `json:"id" gorm:"primaryKey"`
// 	Code     string     `json:"code"`
// 	Name     string     `json:"name"`
// 	Teachers []*Teacher `json:"teachers" gorm:"many2many:subjects_teachers;"`
// }

// type Teacher struct {
// 	ID       uint       `json:"id" gorm:"primaryKey"`
// 	Name     string     `json:"name"`
// 	Username string     `json:"username"`
// 	Email    string     `json:"email"`
// 	Subjects []*Subject `json:"subjects" gorm:"many2many:subjects_teachers;"`
// 	Courses  []*Course  `json:"courses" gorm:"many2many:courses_teachers;"`
// 	Password string     `json:"-"`
// }

// type TeacherLogin struct {
// 	Email    string `json:"email"`
// 	Password string `json:"password"`
// }
