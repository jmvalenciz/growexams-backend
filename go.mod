module growexams/growexams-backend

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
