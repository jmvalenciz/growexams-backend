package service

import (
	"growexams/growexams-backend/db"
	"growexams/growexams-backend/graph/model"

	"github.com/dgrijalva/jwt-go"

	"os"
	"time"
)

type claims struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	UserType string `json:"userType"`
	jwt.StandardClaims
}

var jwtKey = []byte(os.Getenv("JWT_KEY"))

// GenJWT ...
func GenJWT(username string, email string, userType string) string {
	expirationTime := time.Now().Add(15 * time.Minute)

	claim := &claims{
		Username: username,
		Email:    email,
		UserType: userType,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	stringToken, _ := token.SignedString(jwtKey)
	return stringToken
}

// CheckJWT ...
func CheckJWT(token string) (bool, string, string, string) {
	claim := &claims{}
	tkn, err := jwt.ParseWithClaims(token, claim, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		return false, "", "", ""
	}

	return tkn.Valid, claim.Username, claim.Email, claim.UserType
}

// CheckAdminJWT ...
func CheckAdminJWT(token string) bool {
	admin := &[]model.Admin{}
	claim := &claims{}
	_, err := jwt.ParseWithClaims(token, claim, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		return false
	}

	db.MySQLConn.Where("username = ?", claim.Username).Where("email = ?", claim.Email).Find(admin)
	if len(*admin) > 0 {
		return true
	}
	return false
}
