package db

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"growexams/growexams-backend/graph/model"
)

// MySQLConn ...
var MySQLConn *gorm.DB

// InitMySQL ...
func InitMySQL() {
	err := godotenv.Load()
	if err != nil {
		panic("Error loading .env file")
	}
	dbURI := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		os.Getenv("MYSQL_USER"),
		os.Getenv("MYSQL_PASSWORD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_PORT"),
		os.Getenv("MYSQL_DB"),
	)
	conn, connErr := gorm.Open(mysql.Open(dbURI), &gorm.Config{})

	if connErr != nil {
		panic("Failed to connect to database!")
	}

	conn.AutoMigrate(&model.ExamResult{})
	conn.AutoMigrate(&model.Answer{})
	conn.AutoMigrate(&model.Question{})
	conn.AutoMigrate(&model.Exam{})
	conn.AutoMigrate(&model.Admin{})
	conn.AutoMigrate(&model.Student{})
	conn.AutoMigrate(&model.Teacher{})
	conn.AutoMigrate(&model.Subject{})
	conn.AutoMigrate(&model.Course{})

	MySQLConn = conn
}
